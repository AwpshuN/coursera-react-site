//dependencies
import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";

// Components
import Menu from "./Menu";
import Header from "./Header";
import Footer from "./Footer";
import Home from "./Home";
import Contact from './Contact';
import DishDetail from './DishdetailComponent';

// Data
import { DISHES } from "./shared/dishes";
import { COMMENTS } from "./shared/comments";
import { LEADERS } from "./shared/leaders";
import { PROMOTIONS } from "./shared/promotions";
import About from './About';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Dishes: DISHES,
      comments: COMMENTS,
      promotions: PROMOTIONS,
      leaders: LEADERS
    };
  };

  

  render() {

    const HomePage = () => {
      return (
        <Home dish={this.state.Dishes.filter((dish)=> dish.featured)[0]}
          leader={this.state.leaders.filter((leader)=> leader.featured)[0]}
          promotion={this.state.promotions.filter((promo)=> promo.featured)[0] } />
        );
    }

    const DishWithID = ({ match }) => {
      return (
        <DishDetail dish={this.state.Dishes.filter((dish)=> dish.id === parseInt(match.params.dishId, 10))[0]}
        comments={this.state.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId, 10))} />
      );
    };

    return (
      <>
        <Header />
          <Switch>
            <Route path='/home' component={HomePage} />
            <Route exact path='/menu' component={() => <Menu dishes={this.state.Dishes} />} />
            <Route path='/menu/:dishId' component={DishWithID} />
            <Route path='/aboutus' component={() => <About leaders={this.state.leaders} />} />
            <Route exact path='/contactus' component={Contact} />
            <Redirect to='/home' />
          </Switch>
        <Footer />
      </>
    );
  }
}

export default Main;

