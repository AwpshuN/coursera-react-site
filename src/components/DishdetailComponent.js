import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from "react-router-dom";

const DishDetail = ({ dish, comments }) => {

    const renderDish = () => {
        if (dish != null) {
            return (
                <div className="col-12 col-md-5 m-1">
                    <Card >
                        <CardImg bottom height='50%' src={dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Card>
                </div>
            )
        }
        else 
            return <div></div>
            
    }

    const renderComments = () => {
        if (dish != null) {
            return (
                <div className="col-12 col-md-5 m-1"> 
                    <h4>Comments:</h4>
                    {
                        comments.map((dishComment) => (
                            <div className="container">
                                <ul key={dishComment.id} className="list-unstyled">
                                    <li className="Lead">{dishComment.comment}</li>
                                    <li>--{dishComment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit' }).format(new Date(Date.parse(dishComment.date)))}</li>
                                </ul>
                            </div>
                        ))
                    }
                </div>
            )
        }
        else 
            return <div></div>
    }



    return (
        <div className='container'>
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to='/menu' >Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{dish.name}</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>{dish.name}</h3>
                    <hr/>
                </div>
            </div>
            <div className="row">
                {renderDish()}
                {renderComments()}
            </div>
        </div>
    );
}

export default DishDetail;