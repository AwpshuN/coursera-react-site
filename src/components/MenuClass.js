// Menu Component made based on Class Component

import React, { Component } from 'react';
import { Card, CardImg, CardTitle, CardImgOverlay } from 'reactstrap';

class MenuClass extends Component {
    constructor(props) {
        super(props);
        console.log('Menu Component constructor invoked')
    }

    componentDidMount = () => {
        console.log('Menu ComponentDidMount invoked')
    };



    render() { 
        const menu = this.props.dishes.map((dish)=> {
            return (
                <div  className="col-12 col-md-5 m-1" key={dish.id}>
                <Card
                  onClick={() => this.props.onClick(dish.id)}>
                  <CardImg width="100%" src={dish.image} alt={dish.name} />
                  <CardImgOverlay>
                      <CardTitle>{dish.name}</CardTitle>
                  </CardImgOverlay>
                </Card>
              </div>
            )
        })
        console.log('Menu Component render invoked')

        return ( 
            <div className="container">
                <div className="row">
                    {menu}
                </div>
                <div className="row justify-content-center">
                    {/* {this.renderDish(this.state.selectedDish)} */}
                    {/* <DishDetail dish={this.state.selectedDish} /> */}
                </div>
            </div>
         );
    }
}
 
export default MenuClass;